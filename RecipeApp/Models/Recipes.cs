﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApp.Models
{
    public class Recipes
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public int User_Id { get; set; }
        public string RecipeName { get; set; }
        public DateTime Date { get; set; }
        public int Visits { get; set; }
        public string Description { get; set; }
    }
}
