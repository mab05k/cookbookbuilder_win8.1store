﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApp.Models
{
    public class Ingredients
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public int Recipes_Id { get; set; }
        public string IngredientType { get; set; }
        public string IngredientName { get; set; }
        public string IngredientMeasurement { get; set; }
        public string IngredientAmount { get; set; }
    }

    public class AllIngredints
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string SuggestedAmount { get; set; }
        public string SuggestedMeasurement { get; set; }
    }

    public class FoodCategory
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public string Category { get; set; }
    }

    public class IngredientTypeSelection
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string SuggestedAmount { get; set; }
        public string SuggestedMeasurement { get; set; }
    }
    public class Measurement
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Meas { get; set; }
    }
}
