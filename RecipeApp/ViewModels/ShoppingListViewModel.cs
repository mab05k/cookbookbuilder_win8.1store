﻿using RecipeApp.Models;
using RecipeApp.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApp.ViewModels
{
    public class ShoppingListViewModel : ViewModelBase
    {
        #region Properties, Getters, & Setters
        private ObservableCollection<IngredientViewModel> meat;
        private ObservableCollection<IngredientViewModel> vegetable;
        private ObservableCollection<IngredientViewModel> fruit;
        private ObservableCollection<IngredientViewModel> grain;
        private ObservableCollection<IngredientViewModel> nuts;
        private ObservableCollection<IngredientViewModel> dairy;
        private ObservableCollection<IngredientViewModel> herbs;
        private ObservableCollection<IngredientViewModel> legumes;
        private ObservableCollection<IngredientViewModel> oils;
        private ObservableCollection<IngredientViewModel> vinegarsstocks;
        private ObservableCollection<IngredientViewModel> specialty;

        public ObservableCollection<IngredientViewModel> Meat
        {
            get { return this.meat; }
            set
            {
                this.meat = value;
                RaisePropertyChanged("Meat");
            }
        }
        public ObservableCollection<IngredientViewModel> Vegetable
        {
            get { return this.vegetable; }
            set
            {
                this.vegetable = value;
                RaisePropertyChanged("Vegetable");
            }
        }
        public ObservableCollection<IngredientViewModel> Fruit
        {
            get { return this.fruit; }
            set
            {
                this.fruit = value;
                RaisePropertyChanged("Fruit");
            }
        }
        public ObservableCollection<IngredientViewModel> Grain
        {
            get { return this.grain; }
            set
            {
                this.grain = value;
                RaisePropertyChanged("Grain");
            }
        }
        public ObservableCollection<IngredientViewModel> Nuts
        {
            get { return this.nuts; }
            set
            {
                this.nuts = value;
                RaisePropertyChanged("Nuts");
            }
        }
        public ObservableCollection<IngredientViewModel> Dairy
        {
            get { return this.dairy; }
            set
            {
                this.dairy = value;
                RaisePropertyChanged("Dairy");
            }
        }
        public ObservableCollection<IngredientViewModel> Herbs
        {
            get { return this.herbs; }
            set
            {
                this.herbs = value;
                RaisePropertyChanged("Herbs");
            }
        }
        public ObservableCollection<IngredientViewModel> Legumes
        {
            get { return this.legumes; }
            set
            {
                this.legumes = value;
                RaisePropertyChanged("Legumes");
            }
        }
        public ObservableCollection<IngredientViewModel> Oils
        {
            get { return this.oils; }
            set
            {
                this.oils = value;
                RaisePropertyChanged("Oils");
            }
        }
        public ObservableCollection<IngredientViewModel> VinegarStocks
        {
            get { return this.vinegarsstocks; }
            set
            {
                this.vinegarsstocks = value;
                RaisePropertyChanged("VinegarStocks");
            }
        }
        public ObservableCollection<IngredientViewModel> Specialty
        {
            get { return this.specialty; }
            set
            {
                this.specialty = value;
                RaisePropertyChanged("Specialty");
            }
        }
        #endregion

        private void ClearCollections()
        {
            Meat.Clear();
            Vegetable.Clear();
            Fruit.Clear();
            Grain.Clear();
            Nuts.Clear();
            Dairy.Clear();
            Herbs.Clear();
            Legumes.Clear();
            Oils.Clear();
            VinegarStocks.Clear();
            Specialty.Clear();
        }
        public ShoppingListViewModel CreateShoppingList(ObservableCollection<RecipeViewModel> recipes)
        {
            var shoppingList = new ShoppingListViewModel();

            shoppingList.Meat = new ObservableCollection<IngredientViewModel>();
            shoppingList.Vegetable = new ObservableCollection<IngredientViewModel>();
            shoppingList.Fruit = new ObservableCollection<IngredientViewModel>();
            shoppingList.Grain = new ObservableCollection<IngredientViewModel>();
            shoppingList.Nuts = new ObservableCollection<IngredientViewModel>();
            shoppingList.Dairy = new ObservableCollection<IngredientViewModel>();
            shoppingList.Herbs = new ObservableCollection<IngredientViewModel>();
            shoppingList.Legumes = new ObservableCollection<IngredientViewModel>();
            shoppingList.Oils = new ObservableCollection<IngredientViewModel>();
            shoppingList.VinegarStocks = new ObservableCollection<IngredientViewModel>();
            shoppingList.Specialty = new ObservableCollection<IngredientViewModel>();

            //ClearCollections();

            #region IngredientsLoop
            for (int i = 0; i < recipes.Count; i++)
            {
                for (int j = 0; j < recipes[i].Ingredients.Count; j++)
                {
                    switch (recipes[i].Ingredients[j].SelectedCategory.Category)
                    {
                        case "Meat":
                            AddIngredientsSpecific(shoppingList.Meat, recipes[i].Ingredients[j]);
                            break;
                        case "Vegetables":
                            AddIngredientsSpecific(shoppingList.Vegetable, recipes[i].Ingredients[j]);
                            break;
                        case "Fruit":
                            AddIngredientsSpecific(shoppingList.Fruit, recipes[i].Ingredients[j]);
                            break;
                        case "Grain":
                            AddIngredientsGeneric(shoppingList.Grain, recipes[i].Ingredients[j]);
                            break;
                        case "Nuts":
                            AddIngredientsSpecific(shoppingList.Nuts, recipes[i].Ingredients[j]);
                            break;
                        case "Dairy":
                            AddIngredientsSpecific(shoppingList.Dairy, recipes[i].Ingredients[j]);
                            break;
                        case "Herbs & Spices":
                            AddIngredientsGeneric(shoppingList.Herbs, recipes[i].Ingredients[j]);
                            break;
                        case "Legumes":
                            AddIngredientsSpecific(shoppingList.Legumes, recipes[i].Ingredients[j]);
                            break;
                        case "Oils":
                            AddIngredientsGeneric(shoppingList.Oils, recipes[i].Ingredients[j]);
                            break;
                        case "Vinegars & Stocks":
                            AddIngredientsGeneric(shoppingList.VinegarStocks, recipes[i].Ingredients[j]);
                            break;
                        case "Specialty Item":
                            AddIngredientsGeneric(shoppingList.Specialty, recipes[i].Ingredients[j]);
                            break;
                        default:
                            break;
                    }
                }
            }
            #endregion

            shoppingList.Meat = new ObservableCollection<IngredientViewModel>(shoppingList.Meat.OrderBy(i => i.IngredientName));
            shoppingList.Vegetable = new ObservableCollection<IngredientViewModel>(shoppingList.Vegetable.OrderBy(i => i.IngredientName));
            shoppingList.Fruit = new ObservableCollection<IngredientViewModel>(shoppingList.Fruit.OrderBy(i => i.IngredientName));
            shoppingList.Grain = new ObservableCollection<IngredientViewModel>(shoppingList.Grain.OrderBy(i => i.IngredientName));
            shoppingList.Nuts = new ObservableCollection<IngredientViewModel>(shoppingList.Nuts.OrderBy(i => i.IngredientName));
            shoppingList.Dairy = new ObservableCollection<IngredientViewModel>(shoppingList.Dairy.OrderBy(i => i.IngredientName));
            shoppingList.Herbs = new ObservableCollection<IngredientViewModel>(shoppingList.Herbs.OrderBy(i => i.IngredientName));
            shoppingList.Legumes = new ObservableCollection<IngredientViewModel>(shoppingList.Legumes.OrderBy(i => i.IngredientName));
            shoppingList.Oils = new ObservableCollection<IngredientViewModel>(shoppingList.Oils.OrderBy(i => i.IngredientName));
            shoppingList.VinegarStocks = new ObservableCollection<IngredientViewModel>(shoppingList.VinegarStocks.OrderBy(i => i.IngredientName));
            shoppingList.Specialty = new ObservableCollection<IngredientViewModel>(shoppingList.Specialty.OrderBy(i => i.IngredientName));

            return shoppingList;
        }
        private void AddIngredientsSpecific(ObservableCollection<IngredientViewModel> Food, IngredientViewModel ingredient)
        {
            if (Food.Count == 0)
                //Food.Add(new IngredientViewModel(ingredient.Id, ingredient.Recipes_Id, ingredient.IngredientType, ingredient.IngredientName,
                //    ingredient.IngredientMeasurement, ingredient.IngredientAmount));
                Food.Add(new IngredientViewModel().GetIngredient(ingredient.Id));
            else
            {
                for (int i = 0; i < Food.Count; i++)
                {
                    if (Food[i].IngredientName == ingredient.IngredientName)
                    {
                        if (Food[i].IngredientMeasurement == ingredient.IngredientMeasurement)
                        {
                            Food[i].IngredientAmount = (double.Parse(UtilityClass.FormatToDecimal(Food[i].IngredientAmount)) + double.Parse(UtilityClass.FormatToDecimal(ingredient.IngredientAmount))).ToString();
                            return;
                        }
                        #region LB:OZ
                        // LB:OZ
                        else if (Food[i].IngredientMeasurement == "Lb." && ingredient.IngredientMeasurement == "Oz.")
                        {
                            Food[i].IngredientAmount = MeasurementConversions.OzToPound(UtilityClass.FormatToDecimal(Food[i].IngredientAmount), UtilityClass.FormatToDecimal(ingredient.IngredientAmount));
                            return;
                        } // OZ:LB
                        else if (Food[i].IngredientMeasurement == "Oz." && ingredient.IngredientMeasurement == "Lb.")
                        {
                            Food[i].IngredientAmount = MeasurementConversions.OzToPound(UtilityClass.FormatToDecimal(ingredient.IngredientAmount), UtilityClass.FormatToDecimal(Food[i].IngredientAmount));
                            Food[i].IngredientMeasurement = "Lb.";
                            return;
                        }
                        #endregion
                        #region COUNT:CLOVE
                        // COUNT:CLOVE
                        else if (Food[i].IngredientMeasurement == "Count" && ingredient.IngredientMeasurement == "Clove")
                        {
                            Food[i].IngredientAmount = (double.Parse(UtilityClass.FormatToDecimal(Food[i].IngredientAmount)) + 1).ToString();
                            return;
                        } // CLOVE:COUNT
                        else if (Food[i].IngredientMeasurement == "Clove" && ingredient.IngredientMeasurement == "Count")
                        {
                            Food[i].IngredientMeasurement = "Count";
                            Food[i].IngredientAmount = (double.Parse(UtilityClass.FormatToDecimal(ingredient.IngredientAmount)) + 1).ToString();
                            return;
                        }
                        #endregion
                        #region COUNT:CUPS
                        // COUNT:CUPS
                        else if (Food[i].IngredientMeasurement == "Count" && ingredient.IngredientMeasurement == "Cups")
                        {
                            if (double.Parse(UtilityClass.FormatToDecimal(ingredient.IngredientAmount)) >= 4)
                            {
                                Food[i].IngredientAmount = (double.Parse(UtilityClass.FormatToDecimal(Food[i].IngredientAmount)) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        } // CUPS:COUNT
                        else if (Food[i].IngredientMeasurement == "Cups" && ingredient.IngredientMeasurement == "Count")
                        {
                            if (double.Parse(UtilityClass.FormatToDecimal(Food[i].IngredientAmount)) >= 4)
                            {
                                Food[i].IngredientMeasurement = "Count";
                                Food[i].IngredientAmount = (double.Parse(UtilityClass.FormatToDecimal(ingredient.IngredientAmount)) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        }
                        #endregion
                        #region COUNT:SLICE
                        // COUNT:SLICE
                        else if (Food[i].IngredientMeasurement == "Count" && ingredient.IngredientMeasurement == "Slice")
                        {
                            if (double.Parse(UtilityClass.FormatToDecimal(ingredient.IngredientAmount)) >= 4)
                            {
                                Food[i].IngredientAmount = (double.Parse(Food[i].IngredientAmount) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        } // SLICE:COUNT
                        else if (Food[i].IngredientMeasurement == "Slice" && ingredient.IngredientMeasurement == "Count")
                        {
                            if (double.Parse(UtilityClass.FormatToDecimal(Food[i].IngredientAmount)) >= 4)
                            {
                                Food[i].IngredientMeasurement = "Count";
                                Food[i].IngredientAmount = (double.Parse(UtilityClass.FormatToDecimal(ingredient.IngredientAmount)) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        }
                        #endregion
                        #region GALLON:CUP
                        // GALLON:CUP
                        else if (Food[i].IngredientMeasurement == "Gallon" && ingredient.IngredientMeasurement == "Cup")
                        {
                            Food[i].IngredientAmount = MeasurementConversions.OzToPound(UtilityClass.FormatToDecimal(Food[i].IngredientAmount), UtilityClass.FormatToDecimal(ingredient.IngredientAmount));
                            return;
                        } // CUP:GALLON
                        else if (Food[i].IngredientMeasurement == "Cup" && ingredient.IngredientMeasurement == "Gallon")
                        {
                            Food[i].IngredientAmount = MeasurementConversions.OzToPound(UtilityClass.FormatToDecimal(ingredient.IngredientAmount), UtilityClass.FormatToDecimal(Food[i].IngredientAmount));
                            Food[i].IngredientMeasurement = "Gallon";
                            return;
                        }
                        #endregion
                        #region TBSP:TSP
                        // TBSP:TSP
                        else if (Food[i].IngredientMeasurement == "Tbsp" && ingredient.IngredientMeasurement == "Tsp")
                        {
                            Food[i].IngredientAmount = MeasurementConversions.TbspPlusTsp(UtilityClass.FormatToDecimal(Food[i].IngredientAmount), UtilityClass.FormatToDecimal(ingredient.IngredientAmount));
                            return;
                        } // TSP:TBSP
                        else if (Food[i].IngredientMeasurement == "Tsp" && ingredient.IngredientMeasurement == "Tbsp")
                        {
                            Food[i].IngredientAmount = MeasurementConversions.TbspPlusTsp(UtilityClass.FormatToDecimal(ingredient.IngredientAmount), UtilityClass.FormatToDecimal(Food[i].IngredientAmount));
                            Food[i].IngredientMeasurement = "Tbsp";
                            return;
                        }
                        #endregion
                    }
                }
            //    Food.Add(new Ingredient(ingredient.Id, ingredient.Recipes_ID, ingredient.IngredientType, ingredient.IngredientName,
            //ingredient.IngredientMeasurement, ingredient.IngredientAmount));
                Food.Add(new IngredientViewModel().GetIngredient(ingredient.Id));
            }
        }
        private void AddIngredientsGeneric(ObservableCollection<IngredientViewModel> Food, IngredientViewModel ingredient)
        {
            if (Food.Count == 0)
                //Food.Add(new Ingredient(ingredient.Id, ingredient.Recipes_ID, ingredient.IngredientType, ingredient.IngredientName,
                //    ingredient.IngredientMeasurement, ingredient.IngredientAmount));
                Food.Add(new IngredientViewModel().GetIngredient(ingredient.Id));
            else
            {
                for (int i = 0; i < Food.Count; i++)
                    if (Food[i].IngredientName == ingredient.IngredientName)
                        return;

                //Food.Add(new Ingredient(ingredient.Id, ingredient.Recipes_ID, ingredient.IngredientType, ingredient.IngredientName,
                //    ingredient.IngredientMeasurement, ingredient.IngredientAmount));
                Food.Add(new IngredientViewModel().GetIngredient(ingredient.Id));
            }
        }
    }
}
