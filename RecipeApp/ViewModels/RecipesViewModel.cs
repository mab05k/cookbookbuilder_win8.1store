﻿using RecipeApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RecipeApp.ViewModels
{
    public class RecipesViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ObservableCollection<RecipeViewModel> recipes;
        public ObservableCollection<RecipeViewModel> Recipes
        {
            get { return recipes; }
            set
            {
                recipes = value;
                RaisePropertyChanged("Recipes");
            }
        }

        private RecipeApp.App app = (Application.Current as App);
        public ObservableCollection<RecipeViewModel> GetRecipes(int user_id)
        {
            recipes = new ObservableCollection<RecipeViewModel>();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var query = db.Table<Recipes>().Where(c => c.User_Id == user_id);
                foreach (var recipe in query)
                {
                    var _recipe = new RecipeViewModel().GetRecipe(recipe.Id);
                    recipes.Add(_recipe);
                }
            }
            return recipes;
        }
    }
}
