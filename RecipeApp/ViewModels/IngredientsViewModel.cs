﻿using RecipeApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RecipeApp.ViewModels
{
    public class IngredientsViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ObservableCollection<IngredientViewModel> ingredients;

        public ObservableCollection<IngredientViewModel> Ingredients
        {
            get { return ingredients; }
            set
            {
                ingredients = value;
                RaisePropertyChanged("Ingredients");
            }
        }

        private RecipeApp.App app = (Application.Current as App);

        public ObservableCollection<IngredientViewModel> GetIngredients(int recipe_id)
        {
            ingredients = new ObservableCollection<IngredientViewModel>();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var query = db.Table<Ingredients>().Where(c => c.Recipes_Id == recipe_id);
                foreach (var ingredient in query)
                {
                    var _ingredient = new IngredientViewModel().GetIngredient(ingredient.Id);
                    ingredients.Add(_ingredient);
                }
            }
            return ingredients;
        }
    }
    public class FoodCategorysViewModel : ViewModelBase
    {
        private ObservableCollection<FoodCategoryViewModel> foodCategory;
        public ObservableCollection<FoodCategoryViewModel> FoodCategory
        {
            get { return foodCategory; }
            set
            {
                foodCategory = value;
                RaisePropertyChanged("FoodCategory");
            }
        }

        private RecipeApp.App app = (Application.Current as App);
        public ObservableCollection<FoodCategoryViewModel> GetFoodCategories()
        {
            foodCategory = new ObservableCollection<FoodCategoryViewModel>();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var query = db.Table<FoodCategory>();
                foreach (var category in query)
                {
                    var _category = new FoodCategoryViewModel()
                    {
                        Id = category.Id,
                        Category = category.Category
                    };
                    foodCategory.Add(_category);
                }
            }
            return foodCategory;
        }
    }
    public class IngredientTypeSelectionsViewModel : ViewModelBase
    {
        private ObservableCollection<IngredientTypeSelectionViewModel> ingredientTypeSelection;
        public ObservableCollection<IngredientTypeSelectionViewModel> IngredientTypeSelection
        {
            get { return ingredientTypeSelection; }
            set
            {
                ingredientTypeSelection = value;
                RaisePropertyChanged("IngredientTypeSelection");
            }
        }

        private RecipeApp.App app = (Application.Current as App);
        public ObservableCollection<IngredientTypeSelectionViewModel> GetIngredientTypesByCategory(string category)
        {
            category = ConvertCategoryNames(category);
            ingredientTypeSelection = new ObservableCollection<IngredientTypeSelectionViewModel>();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var query = (db.Table<AllIngredints>()).OrderBy(c => c.Name);
                if (category != "")
                {
                    //query = (db.Table<AllIngredints>().Where(c => c.Type == category)).OrderBy(c => c.Name);
                    query = query.Where(c => c.Type == category).OrderBy(c => c.Name);
                }
                foreach (var item in query)
                {
                    var ingredient = new IngredientTypeSelectionViewModel();
                    ingredient.Id = item.Id;
                    ingredient.Type = item.Type;
                    ingredient.Name = item.Name;
                    ingredient.SuggestedAmount = item.SuggestedAmount;
                    ingredient.SuggestedMeasurement = item.SuggestedMeasurement;

                    ingredientTypeSelection.Add(ingredient);
                }
            }
            return ingredientTypeSelection;
        }
    }
    public class MeasurementsViewModel : ViewModelBase
    {
        private ObservableCollection<MeasurementViewModel> measurement;
        public ObservableCollection<MeasurementViewModel> Measurement
        {
            get { return measurement; }
            set
            {
                measurement = value;
                RaisePropertyChanged("Measurement");
            }
        }

        private RecipeApp.App app = (Application.Current as App);

        public ObservableCollection<MeasurementViewModel> GetMeasurements(string category)
        {
            category = ConvertCategoryNames(category);
            measurement = new ObservableCollection<MeasurementViewModel>();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var query = db.Table<Measurement>().Where(c => c.Type == category);
                foreach (var item in query)
                {
                    var _measurement = new MeasurementViewModel();
                    _measurement.Id = item.Id;
                    _measurement.Type = item.Type;
                    _measurement.Measurement = item.Meas;
                    measurement.Add(_measurement);
                }
            }
            return measurement;
        }
    }
}
