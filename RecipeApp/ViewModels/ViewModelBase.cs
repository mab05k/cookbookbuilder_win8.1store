﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApp.ViewModels
{
    public class ViewModelBase
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string ConvertCategoryNames(string category)
        {
            switch (category)
            {
                case "Vegetables":
                    category = "Vegetable";
                    break;
                case "Grains":
                    category = "Grain";
                    break;
                case "Nuts":
                    category = "Nut";
                    break;
                case "Herbs & Spices":
                    category = "Herb";
                    break;
                case "Oils":
                    category = "Oil";
                    break;
                case "Legumes":
                    category = "Legume";
                    break;
                case "Vinegars & Stocks":
                    category = "Vinegar";
                    break;
                case "Specialty Items":
                    category = "Specialty";
                    break;
                default:
                    break;
            }
            return category;
        }

    }
}
