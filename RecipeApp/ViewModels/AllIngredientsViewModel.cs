﻿using RecipeApp.Models;
using RecipeApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RecipeApp.ViewModels
{
    public class AllIngredientsViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string SuggestedAmount { get; set; }
        public string SuggestedMeasurement { get; set; }

        private RecipeApp.App app = (Application.Current as App);

        public string SaveIngredient(AllIngredientsViewModel newIngredient)
        {
            string success = string.Empty;
            try
            {
                using (var db = new SQLite.SQLiteConnection(app.DBPath))
                {
                    bool exists = db.Table<AllIngredints>().Any(c => c.Name == newIngredient.Name);
                    if (!exists)
                    {
                        var _newIngredient = new AllIngredints();
                        _newIngredient.Id = newIngredient.GetNewIngredientId();
                        _newIngredient.Type = UtilityClass.GetCategory(newIngredient.Type);
                        var firstLetter = newIngredient.Name.Substring(0, 1).ToUpper();
                        var lastLetters = newIngredient.Name.Substring(1);
                        _newIngredient.Name = firstLetter + lastLetters;
                        _newIngredient.SuggestedAmount = "1";
                        _newIngredient.SuggestedMeasurement = newIngredient.SuggestedMeasurement;
                        _newIngredient.SuggestedMeasurement = new MeasurementsViewModel().GetMeasurements(_newIngredient.Type)[0].Measurement;

                        db.Insert(_newIngredient);
                        success = "Ingredient added successfully.";
                    }
                    else
                    {
                        var existingIngredient = db.Table<AllIngredints>().Where(c => c.Name == newIngredient.Name).Single();
                        success = string.Format("Ingredient already exists:\nCategory: {0}\nIngredient: {1}", existingIngredient.Type, existingIngredient.Name);
                    }
                }
            }
            catch (Exception)
            {
                success = "Ingredient failed to save.";
            }
            return success;
        }
        public bool DeleteIngredient (int ingredient_id)
        {
            bool result = false;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var ingredient = (db.Table<AllIngredints>().Where(c => c.Id == ingredient_id)).SingleOrDefault();
                if (db.Delete(ingredient) > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }
        public int GetNewIngredientId()
        {
            int ingredientId = 0;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var query = db.Table<AllIngredints>();
                if (query.Count() > 0)
                    ingredientId = query.Count() + 2;
                else
                    ingredientId = 1;
            }
            return ingredientId;
        }
    }
}
