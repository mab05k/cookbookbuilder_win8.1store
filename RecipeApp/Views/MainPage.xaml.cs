﻿using RecipeApp.Common;
using RecipeApp.ViewModels;
using RecipeApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace RecipeApp.Views
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        UsersViewModel usersViewModel = null;
        private ObservableCollection<UserViewModel> users;
        public ObservableCollection<UserViewModel> Users
        {
            get { return users; }
            set
            {
                users = value;
            }
        }



        private RecipeApp.App app = (Application.Current as App);

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public MainPage()
        {
            AddSomeData();

            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            users = app.Users;
            UserComboBox.DataContext = users;

            if (e.PageState != null && e.PageState.ContainsKey("selectedUser"))
                UserComboBox.SelectedItem = users.First(c => c.Id == ((UserViewModel)e.PageState["selectedUser"]).Id);
            else
                PrimaryWorkFrame.Navigate(typeof(BlankPage)); 
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            e.PageState["selectedUser"] = UserComboBox.SelectedItem;
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        public void AddSomeData()
        {
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                //db.DeleteAll<Ingredients>();
                //db.DeleteAll<Recipes>();
                //db.DeleteAll<User>();
                #region Test Users and Ingredients added to database
                //db.DeleteAll<User>();
                //db.Insert(new User()
                //{
                //    Id = 1,
                //    UserName = "Mike",
                //    UserSince = DateTime.Now
                //});
                //db.DeleteAll<Ingredients>();
                //db.DeleteAll<Recipes>();
                //db.DeleteAll<AllIngredints>();
                #region Dairy
                //db.Insert(new AllIngredints()
                //    {
                //        Id = 1,
                //        Type = "Dairy",
                //        Name = "Baked Milk",
                //        SuggestedAmount = "1",
                //        SuggestedMeasurement = "Cup"
                //    });
                //db.Insert(new AllIngredints()
                //{
                //    Id = 2,
                //    Type = "Dairy",
                //    Name = "Bulgarian Yogurt",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 3,
                //    Type = "Dairy",
                //    Name = "Butter",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 4,
                //    Type = "Dairy",
                //    Name = "Buttermilk",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 5,
                //    Type = "Dairy",
                //    Name = "Casein",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 6,
                //    Type = "Dairy",
                //    Name = "Condensed Milk",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 7,
                //    Type = "Dairy",
                //    Name = "Cottage Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 8,
                //    Type = "Dairy",
                //    Name = "Cream",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 9,
                //    Type = "Dairy",
                //    Name = "Custard",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 10,
                //    Type = "Dairy",
                //    Name = "Evaporated Milk",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 11,
                //    Type = "Dairy",
                //    Name = "Frozen Yogurt",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 12,
                //    Type = "Dairy",
                //    Name = "Greek Yogurt",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 13,
                //    Type = "Dairy",
                //    Name = "Ice Cream",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 14,
                //    Type = "Dairy",
                //    Name = "Milk",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 15,
                //    Type = "Dairy",
                //    Name = "Powdered Milk",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 16,
                //    Type = "Dairy",
                //    Name = "Sour Cream",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 17,
                //    Type = "Dairy",
                //    Name = "Whipped Cream",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 18,
                //    Type = "Dairy",
                //    Name = "Yogurt",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 19,
                //    Type = "Dairy",
                //    Name = "Bergenost",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 20,
                //    Type = "Dairy",
                //    Name = "Brick Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 21,
                //    Type = "Dairy",
                //    Name = "Cheese Curds",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 22,
                //    Type = "Dairy",
                //    Name = "Colby Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 23,
                //    Type = "Dairy",
                //    Name = "Colby-Jack Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 24,
                //    Type = "Dairy",
                //    Name = "Cream Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 25,
                //    Type = "Dairy",
                //    Name = "Blue Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 26,
                //    Type = "Dairy",
                //    Name = "Monterey-Jack Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 27,
                //    Type = "Dairy",
                //    Name = "Muenster Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 28,
                //    Type = "Dairy",
                //    Name = "Pepper Jack Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 29,
                //    Type = "Dairy",
                //    Name = "String Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 30,
                //    Type = "Dairy",
                //    Name = "Swiss Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 31,
                //    Type = "Dairy",
                //    Name = "Limburger Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 32,
                //    Type = "Dairy",
                //    Name = "Gruyere",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 33,
                //    Type = "Dairy",
                //    Name = "Brie",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 34,
                //    Type = "Dairy",
                //    Name = "Feta Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 35,
                //    Type = "Dairy",
                //    Name = "Parmesan Cheese",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                #endregion
                #region Fruit
                //db.Insert(new AllIngredints()
                //{
                //    Id = 36,
                //    Type = "Fruit",
                //    Name = "Apple",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 37,
                //    Type = "Fruit",
                //    Name = "Banana",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 38,
                //    Type = "Fruit",
                //    Name = "Orange",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 39,
                //    Type = "Fruit",
                //    Name = "Pear",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 40,
                //    Type = "Fruit",
                //    Name = "Strawberry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 41,
                //    Type = "Fruit",
                //    Name = "Blueberry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 42,
                //    Type = "Fruit",
                //    Name = "Mango",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 43,
                //    Type = "Fruit",
                //    Name = "Acai",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 44,
                //    Type = "Fruit",
                //    Name = "Grape",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 45,
                //    Type = "Fruit",
                //    Name = "Avocado",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 46,
                //    Type = "Fruit",
                //    Name = "Plum",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 47,
                //    Type = "Fruit",
                //    Name = "Raspberry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 48,
                //    Type = "Fruit",
                //    Name = "Apricot",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 49,
                //    Type = "Fruit",
                //    Name = "Beach Plum",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 50,
                //    Type = "Fruit",
                //    Name = "Black Cherry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 51,
                //    Type = "Fruit",
                //    Name = "Black Raspberry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 52,
                //    Type = "Fruit",
                //    Name = "Blood Orange",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 53,
                //    Type = "Fruit",
                //    Name = "Cantaloupe",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 54,
                //    Type = "Fruit",
                //    Name = "Cherry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 55,
                //    Type = "Fruit",
                //    Name = "Cranberry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 56,
                //    Type = "Fruit",
                //    Name = "Coconut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 57,
                //    Type = "Fruit",
                //    Name = "Date",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 58,
                //    Type = "Fruit",
                //    Name = "Fig",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 59,
                //    Type = "Fruit",
                //    Name = "Guava",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 60,
                //    Type = "Fruit",
                //    Name = "Grapefruit",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 61,
                //    Type = "Fruit",
                //    Name = "Honeydew",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 62,
                //    Type = "Fruit",
                //    Name = "Juniper Berry",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 63,
                //    Type = "Fruit",
                //    Name = "Kaffir Lime",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 64,
                //    Type = "Fruit",
                //    Name = "Key Lime",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 65,
                //    Type = "Fruit",
                //    Name = "Kiwi",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 66,
                //    Type = "Fruit",
                //    Name = "Lemon",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 67,
                //    Type = "Fruit",
                //    Name = "Lime",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 68,
                //    Type = "Fruit",
                //    Name = "Lychee",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 69,
                //    Type = "Fruit",
                //    Name = "Olive",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 70,
                //    Type = "Fruit",
                //    Name = "Papaya",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 71,
                //    Type = "Fruit",
                //    Name = "Passion Fruit",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 72,
                //    Type = "Fruit",
                //    Name = "Peach",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 73,
                //    Type = "Fruit",
                //    Name = "Pineapple",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 74,
                //    Type = "Fruit",
                //    Name = "Pumpkin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 75,
                //    Type = "Fruit",
                //    Name = "Tangerine",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 76,
                //    Type = "Fruit",
                //    Name = "Watermelon",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 77,
                //    Type = "Fruit",
                //    Name = "Starfruit",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Count"
                //});
                #endregion
                #region Herb
                //db.Insert(new AllIngredints()
                //{
                //    Id = 78,
                //    Type = "Herb",
                //    Name = "Allspice",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 79,
                //    Type = "Herb",
                //    Name = "Salt",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 80,
                //    Type = "Herb",
                //    Name = "Pepper",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 81,
                //    Type = "Herb",
                //    Name = "Angelica",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 82,
                //    Type = "Herb",
                //    Name = "Anise",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 83,
                //    Type = "Herb",
                //    Name = "Basil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 84,
                //    Type = "Herb",
                //    Name = "Bay Leaf",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 85,
                //    Type = "Herb",
                //    Name = "Black Cardamom",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 86,
                //    Type = "Herb",
                //    Name = "Cardamom",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 87,
                //    Type = "Herb",
                //    Name = "Cayenne Pepper",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 88,
                //    Type = "Herb",
                //    Name = "Chili Pepper",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 89,
                //    Type = "Herb",
                //    Name = "Chives",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 90,
                //    Type = "Herb",
                //    Name = "Cilantro",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 91,
                //    Type = "Herb",
                //    Name = "Cinnamon",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 92,
                //    Type = "Herb",
                //    Name = "Cumin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 93,
                //    Type = "Herb",
                //    Name = "Dill Seed",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 94,
                //    Type = "Herb",
                //    Name = "Fennel",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 95,
                //    Type = "Herb",
                //    Name = "Garlic",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Clove"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 96,
                //    Type = "Herb",
                //    Name = "Ginger",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 97,
                //    Type = "Herb",
                //    Name = "Horseradish",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 98,
                //    Type = "Herb",
                //    Name = "Jasmine",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 99,
                //    Type = "Herb",
                //    Name = "Kaffir Lime Leaf",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 100,
                //    Type = "Herb",
                //    Name = "Lemongrass",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 101,
                //    Type = "Herb",
                //    Name = "Mint",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 102,
                //    Type = "Herb",
                //    Name = "Mustard Seed",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 103,
                //    Type = "Herb",
                //    Name = "Nutmeg",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 104,
                //    Type = "Herb",
                //    Name = "Oregano",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 105,
                //    Type = "Herb",
                //    Name = "Paprika",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 106,
                //    Type = "Herb",
                //    Name = "Parsley",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 107,
                //    Type = "Herb",
                //    Name = "Pepper, Black",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 108,
                //    Type = "Herb",
                //    Name = "Rosemary",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 109,
                //    Type = "Herb",
                //    Name = "Safflower",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 110,
                //    Type = "Herb",
                //    Name = "Saffron",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 111,
                //    Type = "Herb",
                //    Name = "Sage",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Sprig"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 112,
                //    Type = "Herb",
                //    Name = "Tarragon",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 113,
                //    Type = "Herb",
                //    Name = "Thyme",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 114,
                //    Type = "Herb",
                //    Name = "Vanilla",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 115,
                //    Type = "Herb",
                //    Name = "Wasabi",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 116,
                //    Type = "Herb",
                //    Name = "Yellow Mustard",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                #endregion
                #region Legume
                //db.Insert(new AllIngredints()
                //{
                //    Id = 117,
                //    Type = "Legume",
                //    Name = "Garbanzo",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 118,
                //    Type = "Legume",
                //    Name = "American Groundnut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 119,
                //    Type = "Legume",
                //    Name = "Azuki Bean",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 120,
                //    Type = "Legume",
                //    Name = "Black-eyed Pea",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 121,
                //    Type = "Legume",
                //    Name = "Chickpea",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 122,
                //    Type = "Legume",
                //    Name = "Common Bean",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 123,
                //    Type = "Legume",
                //    Name = "Fava Bean",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 124,
                //    Type = "Legume",
                //    Name = "Green Bean",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 125,
                //    Type = "Legume",
                //    Name = "Lima Bean",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 126,
                //    Type = "Legume",
                //    Name = "Okra",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 127,
                //    Type = "Legume",
                //    Name = "Pea",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 128,
                //    Type = "Legume",
                //    Name = "Runner Bean",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 129,
                //    Type = "Legume",
                //    Name = "Snap Pea",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 130,
                //    Type = "Legume",
                //    Name = "Snow Pea",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 131,
                //    Type = "Legume",
                //    Name = "Soybean",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                #endregion
                #region Meat
                //db.Insert(new AllIngredints()
                //{
                //    Id = 132,
                //    Type = "Meat",
                //    Name = "Bacon",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 133,
                //    Type = "Meat",
                //    Name = "Chicken Breast",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 134,
                //    Type = "Meat",
                //    Name = "Chicken Thigh",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 135,
                //    Type = "Meat",
                //    Name = "Chicken Wings",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 136,
                //    Type = "Meat",
                //    Name = "Drumsticks",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 137,
                //    Type = "Meat",
                //    Name = "Whole Chicken",
                //    SuggestedAmount = "6",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 138,
                //    Type = "Meat",
                //    Name = "Beef Chuck",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 139,
                //    Type = "Meat",
                //    Name = "Beef Rib",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 140,
                //    Type = "Meat",
                //    Name = "Beef Brisket",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 141,
                //    Type = "Meat",
                //    Name = "Beef Shank",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 142,
                //    Type = "Meat",
                //    Name = "Beef Short Loin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 143,
                //    Type = "Meat",
                //    Name = "Beef Sirloin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 144,
                //    Type = "Meat",
                //    Name = "Beef Tenderloin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 145,
                //    Type = "Meat",
                //    Name = "Beef Top Sirloin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 146,
                //    Type = "Meat",
                //    Name = "Beef Bottom Sirloin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 147,
                //    Type = "Meat",
                //    Name = "Beef Round",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 148,
                //    Type = "Meat",
                //    Name = "Ground Beef",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 149,
                //    Type = "Meat",
                //    Name = "Pork Shoulder",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 150,
                //    Type = "Meat",
                //    Name = "Pork Loin Roast",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 151,
                //    Type = "Meat",
                //    Name = "Ham",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 152,
                //    Type = "Meat",
                //    Name = "Sausage",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 153,
                //    Type = "Meat",
                //    Name = "Pork Back Ribs",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 154,
                //    Type = "Meat",
                //    Name = "Pork Belly",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 155,
                //    Type = "Meat",
                //    Name = "Spare Ribs",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 156,
                //    Type = "Meat",
                //    Name = "Pork Tenderloin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 157,
                //    Type = "Meat",
                //    Name = "Pork Blade Steak",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 158,
                //    Type = "Meat",
                //    Name = "Pork Chops",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 159,
                //    Type = "Meat",
                //    Name = "Grouper",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 160,
                //    Type = "Meat",
                //    Name = "Snapper",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 161,
                //    Type = "Meat",
                //    Name = "Salmon",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 162,
                //    Type = "Meat",
                //    Name = "Tuna",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 163,
                //    Type = "Meat",
                //    Name = "Tuna",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 164,
                //    Type = "Meat",
                //    Name = "Yellowtail",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 165,
                //    Type = "Meat",
                //    Name = "Dolphin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 166,
                //    Type = "Meat",
                //    Name = "Whole Turkey",
                //    SuggestedAmount = "9",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 167,
                //    Type = "Meat",
                //    Name = "Turkey Breast",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 168,
                //    Type = "Meat",
                //    Name = "Turkey Leg",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 169,
                //    Type = "Meat",
                //    Name = "Turkey Leg",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 170,
                //    Type = "Meat",
                //    Name = "Turkey Wings",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Lb."
                //});
                #endregion
                #region Nut
                //db.Insert(new AllIngredints()
                //{
                //    Id = 171,
                //    Type = "Nut",
                //    Name = "Acorn",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 172,
                //    Type = "Nut",
                //    Name = "Beech",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 173,
                //    Type = "Nut",
                //    Name = "Breadnut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 174,
                //    Type = "Nut",
                //    Name = "Chestnut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 175,
                //    Type = "Nut",
                //    Name = "Hazelnut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 176,
                //    Type = "Nut",
                //    Name = "Kola Nut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 177,
                //    Type = "Nut",
                //    Name = "Palm Nut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 178,
                //    Type = "Nut",
                //    Name = "Yellow Walnut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 179,
                //    Type = "Nut",
                //    Name = "Almond",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 180,
                //    Type = "Nut",
                //    Name = "Australian Cashew",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 181,
                //    Type = "Nut",
                //    Name = "Cashew",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 182,
                //    Type = "Nut",
                //    Name = "Coconut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 183,
                //    Type = "Nut",
                //    Name = "Hickory",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 184,
                //    Type = "Nut",
                //    Name = "Jack Nut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 185,
                //    Type = "Nut",
                //    Name = "Pistachio",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 186,
                //    Type = "Nut",
                //    Name = "Walnut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 187,
                //    Type = "Nut",
                //    Name = "Pine Nut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 188,
                //    Type = "Nut",
                //    Name = "Macadamia",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 189,
                //    Type = "Nut",
                //    Name = "Peanut",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Oz."
                //});
                #endregion
                #region Oil
                //db.Insert(new AllIngredints()
                //{
                //    Id = 190,
                //    Type = "Oil",
                //    Name = "Almond Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 191,
                //    Type = "Oil",
                //    Name = "Avocado Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 192,
                //    Type = "Oil",
                //    Name = "Butter",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 193,
                //    Type = "Oil",
                //    Name = "Canola Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 194,
                //    Type = "Oil",
                //    Name = "Coconut Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 195,
                //    Type = "Oil",
                //    Name = "Rice Bran Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 196,
                //    Type = "Oil",
                //    Name = "Corn Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 197,
                //    Type = "Oil",
                //    Name = "Flaxseed Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 198,
                //    Type = "Oil",
                //    Name = "Grape Seed Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 199,
                //    Type = "Oil",
                //    Name = "Lard",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 200,
                //    Type = "Oil",
                //    Name = "Margarine",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 201,
                //    Type = "Oil",
                //    Name = "Macadamia Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 202,
                //    Type = "Oil",
                //    Name = "Olive Oil, Extra Virgin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 203,
                //    Type = "Oil",
                //    Name = "Olive Oil, Virgin",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 204,
                //    Type = "Oil",
                //    Name = "Olive Oil, Extra Light",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 205,
                //    Type = "Oil",
                //    Name = "Palm Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 206,
                //    Type = "Oil",
                //    Name = "Peanut Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 207,
                //    Type = "Oil",
                //    Name = "Sesame Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 208,
                //    Type = "Oil",
                //    Name = "Soybean Oil",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Tbsp"
                //});
                #endregion
                #region Vegetable
                //db.Insert(new AllIngredints()
                //{
                //    Id = 209,
                //    Type = "Vegetable",
                //    Name = "Arugula",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 210,
                //    Type = "Vegetable",
                //    Name = "Green Beans",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 211,
                //    Type = "Vegetable",
                //    Name = "Beet",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 212,
                //    Type = "Vegetable",
                //    Name = "Bok Choy",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 213,
                //    Type = "Vegetable",
                //    Name = "Broccoli Rabe",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 214,
                //    Type = "Vegetable",
                //    Name = "Broccoli",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 215,
                //    Type = "Vegetable",
                //    Name = "Brussels Sprout",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 216,
                //    Type = "Vegetable",
                //    Name = "Cabbage",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 217,
                //    Type = "Vegetable",
                //    Name = "Celery",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 218,
                //    Type = "Vegetable",
                //    Name = "Collard Greens",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 219,
                //    Type = "Vegetable",
                //    Name = "Cress",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 220,
                //    Type = "Vegetable",
                //    Name = "Dill",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 221,
                //    Type = "Vegetable",
                //    Name = "Plantain",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 222,
                //    Type = "Vegetable",
                //    Name = "Kale",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 223,
                //    Type = "Vegetable",
                //    Name = "Lettuce",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 224,
                //    Type = "Vegetable",
                //    Name = "Spinach",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 225,
                //    Type = "Vegetable",
                //    Name = "Napa Cabbage",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 226,
                //    Type = "Vegetable",
                //    Name = "Peas",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 227,
                //    Type = "Vegetable",
                //    Name = "Poke",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 228,
                //    Type = "Vegetable",
                //    Name = "Swiss Chard",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 229,
                //    Type = "Vegetable",
                //    Name = "Turnip",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 230,
                //    Type = "Vegetable",
                //    Name = "Watercress",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 231,
                //    Type = "Vegetable",
                //    Name = "Artichoke",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 232,
                //    Type = "Vegetable",
                //    Name = "Caper",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 234,
                //    Type = "Vegetable",
                //    Name = "Cauliflower",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 235,
                //    Type = "Vegetable",
                //    Name = "Squash",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 236,
                //    Type = "Vegetable",
                //    Name = "Asparagus",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 237,
                //    Type = "Vegetable",
                //    Name = "Chives",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 238,
                //    Type = "Vegetable",
                //    Name = "Leek",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 239,
                //    Type = "Vegetable",
                //    Name = "Onion",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 240,
                //    Type = "Vegetable",
                //    Name = "Pearl Onion",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 241,
                //    Type = "Vegetable",
                //    Name = "Potato Onion",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 242,
                //    Type = "Vegetable",
                //    Name = "Red Onion",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 243,
                //    Type = "Vegetable",
                //    Name = "Shallot",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 244,
                //    Type = "Vegetable",
                //    Name = "Scallion",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 245,
                //    Type = "Vegetable",
                //    Name = "Bamboo Shoot",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 246,
                //    Type = "Vegetable",
                //    Name = "Beetroot",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 247,
                //    Type = "Vegetable",
                //    Name = "Carrot",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 248,
                //    Type = "Vegetable",
                //    Name = "Ginger",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 249,
                //    Type = "Vegetable",
                //    Name = "Potato",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 250,
                //    Type = "Vegetable",
                //    Name = "Radish",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 251,
                //    Type = "Vegetable",
                //    Name = "Sweet Potato",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 252,
                //    Type = "Vegetable",
                //    Name = "Bell Pepper",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 253,
                //    Type = "Vegetable",
                //    Name = "Cucumber",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //}); db.Insert(new AllIngredints()
                //{
                //    Id = 254,
                //    Type = "Vegetable",
                //    Name = "Eggplant",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 255,
                //    Type = "Vegetable",
                //    Name = "Tomato",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                //db.Insert(new AllIngredints()
                //{
                //    Id = 256,
                //    Type = "Vegetable",
                //    Name = "Zucchini",
                //    SuggestedAmount = "1",
                //    SuggestedMeasurement = "Cup"
                //});
                #endregion
                #endregion // Still need to add "Vinegar"
                #region Categories
                //db.DeleteAll<FoodCategory>();
                //db.Insert(new FoodCategory()
                //    {
                //        Id = 1,
                //        Category = "Meat"
                //    });
                //db.Insert(new FoodCategory()
                //{
                //    Id = 2,
                //    Category = "Vegetables"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 3,
                //    Category = "Fruit"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 4,
                //    Category = "Grains"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 5,
                //    Category = "Nuts"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 6,
                //    Category = "Dairy"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 7,
                //    Category = "Herbs & Spices"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 8,
                //    Category = "Legumes"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 9,
                //    Category = "Oils"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 11,
                //    Category = "Vinegars & Stocks"
                //});
                //db.Insert(new FoodCategory()
                //{
                //    Id = 12,
                //    Category = "Specialty Items"
                //});
                #endregion
                #region Measurements
                //db.DeleteAll<Measurement>();
                //db.Insert(new Measurement()
                //{
                //    Id = 1,
                //    Type = "Meat",
                //    Meas = "Lb."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 2,
                //    Type = "Meat",
                //    Meas = "Oz."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 3,
                //    Type = "Vegetable",
                //    Meas = "Count"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 4,
                //    Type = "Vegetable",
                //    Meas = "Clove"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 5,
                //    Type = "Vegetable",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 6,
                //    Type = "Vegetable",
                //    Meas = "Lb."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 7,
                //    Type = "Fruit",
                //    Meas = "Count"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 8,
                //    Type = "Fruit",
                //    Meas = "Slice"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 9,
                //    Type = "Fruit",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 10,
                //    Type = "Grain",
                //    Meas = "Slice"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 11,
                //    Type = "Grain",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 12,
                //    Type = "Grain",
                //    Meas = "Tbsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 13,
                //    Type = "Grain",
                //    Meas = "Tsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 14,
                //    Type = "Nut",
                //    Meas = "Count"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 15,
                //    Type = "Nut",
                //    Meas = "Lb."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 16,
                //    Type = "Nut",
                //    Meas = "Oz."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 17,
                //    Type = "Dairy",
                //    Meas = "Lb."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 18,
                //    Type = "Dairy",
                //    Meas = "Oz."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 19,
                //    Type = "Dairy",
                //    Meas = "Gallon"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 20,
                //    Type = "Dairy",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 21,
                //    Type = "Dairy",
                //    Meas = "Tbsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 22,
                //    Type = "Dairy",
                //    Meas = "Tsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 23,
                //    Type = "Herb",
                //    Meas = "Sprig"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 24,
                //    Type = "Herb",
                //    Meas = "Bunch"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 25,
                //    Type = "Herb",
                //    Meas = "Tbsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 26,
                //    Type = "Herb",
                //    Meas = "Tsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 27,
                //    Type = "Herb",
                //    Meas = "Oz."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 28,
                //    Type = "Legume",
                //    Meas = "Count"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 29,
                //    Type = "Legume",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 30,
                //    Type = "Legume",
                //    Meas = "Lb."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 31,
                //    Type = "Legume",
                //    Meas = "Oz."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 32,
                //    Type = "Oil",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 33,
                //    Type = "Oil",
                //    Meas = "Tbsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 34,
                //    Type = "Oil",
                //    Meas = "Tsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 35,
                //    Type = "Vinegar",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 36,
                //    Type = "Vinegar",
                //    Meas = "Tbsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 37,
                //    Type = "Vinegar",
                //    Meas = "Tsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 38,
                //    Type = "Specialty",
                //    Meas = "Count"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 39,
                //    Type = "Specialty",
                //    Meas = "Lb."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 40,
                //    Type = "Specialty",
                //    Meas = "Oz."
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 41,
                //    Type = "Specialty",
                //    Meas = "Cup"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 42,
                //    Type = "Specialty",
                //    Meas = "Tbsp"
                //});
                //db.Insert(new Measurement()
                //{
                //    Id = 43,
                //    Type = "Specialty",
                //    Meas = "Tsp"
                //});
                #endregion
            }

        }

        private void CreateRecipeButton_Click(object sender, RoutedEventArgs e)
        {
            var recipe = new RecipeViewModel().NewRecipe();
            recipe.Ingredients.Add(new IngredientViewModel().NewIngredient());
            PrimaryWorkFrame.Navigate(typeof(CreateRecipeView), recipe);
        }
        private void CreateIngredientButton_Click(object sender, RoutedEventArgs e)
        {
            var categories = new FoodCategorysViewModel().GetFoodCategories();
            PrimaryWorkFrame.Navigate(typeof(AddIngredientView), categories);
        }
        private void DeleteIngredientButton_Click(object sender, RoutedEventArgs e)
        {
            var ingredient = new IngredientViewModel().NewIngredient();
            PrimaryWorkFrame.Navigate(typeof(DeleteIngredientView), ingredient);
        }

        private async void createNewUserButton_Click(object sender, RoutedEventArgs e)
        {
            if (newUserTextBox.Text != "")
            {
                var newUser = new UserViewModel();
                newUser.GetNewUserId();
                newUser.UserName = newUserTextBox.Text;
                string success = newUser.SaveUser(newUser);

                Users.Add(newUser);
                var message = new MessageDialog("New user created.", "Success!");
                newUserTextBox.Text = "Create New User";

                await message.ShowAsync();
            }
        }

        private void CreateShoppingListButton_Click(object sender, RoutedEventArgs e)
        {
            PrimaryWorkFrame.Navigate(typeof(CreateShoppingList));
        }

        private void UserComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (UserComboBox.SelectedItem != null)
            {
                app.CurrentUser = new UserViewModel().GetUser(((UserViewModel)UserComboBox.SelectedItem).Id);
                this.DataContext = app.CurrentUser;

                PrimaryWorkFrame.Navigate(typeof(SelectedRecipeView), PrimaryWorkFrame);
                CreateRecipeButton.IsEnabled = true;
                CreateShoppingListButton.IsEnabled = true;
            }
        }

        private void DeleteUser_Click(object sender, RoutedEventArgs e)
        {
            PrimaryWorkFrame.Navigate(typeof(DeleteUserView), users);
        }
    }
}
