﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApp.Models
{
    public class User
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public string UserName { get; set; }
        public DateTime UserSince { get; set; }
    }
}
