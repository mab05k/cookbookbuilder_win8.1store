﻿using RecipeApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RecipeApp.ViewModels
{
    public class UserViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private int id = 0;
        private string userName = string.Empty;
        private DateTime userSince = DateTime.Now;
        private ObservableCollection<RecipeViewModel> recipes;
        private ObservableCollection<RecipeViewModel> shoppingList;
        //private RecipeViewModel selectedRecipe = null;

        #region Properties        
        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                { return; }
                id = value;
                RaisePropertyChanged("Id");
            }
        }        
        public string UserName
        {
            get { return userName; }
            set
            {
                if (userName == value)
                { return; }
                userName = value;
                RaisePropertyChanged("UserName");
            }
        }        
        public DateTime UserSince
        {
            get { return userSince; }
            set
            {
                if (userSince == value)
                { return; }
                userSince = value;
                RaisePropertyChanged("UserSince");
            }
        }
        public ObservableCollection<RecipeViewModel> Recipes
        {
            get { return recipes; }
            set
            {
                recipes = value;
                RaisePropertyChanged("Recipes");
            }
        }
        public ObservableCollection<RecipeViewModel> ShoppingList
        {
            get { return shoppingList; }
            set
            {
                shoppingList = value;
                RaisePropertyChanged("ShoppingList");
            }
        }
        #endregion

        private RecipeApp.App app = (Application.Current as App);

        public UserViewModel GetUser(int user_Id)
        {
            var user = new UserViewModel();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var _user = (db.Table<User>().Where(c => c.Id == user_Id)).Single();
                user.Id = _user.Id;
                user.UserName = _user.UserName;
                user.UserSince = _user.UserSince;
                user.Recipes = new RecipesViewModel().GetRecipes(user_Id);
            }
            return user;
        }
        public string GetUserName(int user_Id)
        {
            string userName = "Unknown";
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var user = (db.Table<User>().Where(c => c.Id == user_Id)).Single();
                userName = user.UserName;
            }
            return userName;
        }
        public string SaveUser(UserViewModel user)
        {
            string result = string.Empty;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                try
                {
                    var existingUser = (db.Table<User>().Where(c => c.Id == user.Id)).SingleOrDefault();

                    if (existingUser != null)
                    {
                        existingUser.UserName = user.UserName;
                        int success = db.Update(existingUser);
                    }
                    else
                    {
                        int success = db.Insert(new User()
                            {
                                Id = user.Id,
                                UserName = user.UserName,
                                UserSince = user.UserSince
                            });
                    }
                    result = "Success";
                }
                catch (Exception)
                {
                    result = "This user was not saved.";
                }
            }
            return result;
        }
        public bool DeleteUser(int user_Id)
        {
            bool result = false;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var recipes = db.Table<Recipes>().Where(c => c.Id == user_Id);
                foreach (var recipe in recipes)
                {
                    db.Delete(recipe);
                }

                var existingUser = (db.Table<User>().Where(c => c.Id == user_Id)).Single();

                if (db.Delete(existingUser) > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }
        public int GetNewUserId()
        {
            int userId = 0;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var users = db.Table<User>();
                if (users.Count() > 0)
                {
                    userId = (from c in db.Table<User>()
                              select c.Id).Max();
                    userId += 1;
                }
                else
                {
                    userId = 1;
                }
            }
            return userId;
        }
    }
}
