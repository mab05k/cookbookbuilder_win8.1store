﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApp.Common
{
    // THIS CLASS IS FOR CONVERTING INPUT VALUES FOR RECIPE AMOUNTS INTO FRACTIONS AND DECIMALS (FOR DB STORAGE)

    public class FractionConverter
    {
        public static string ConvertToFraction(string number)  // CONVERTS DECIMAL VALUE (STRING FORMAT) INTO A FRACTION
        {
            try
            {
                decimal value = decimal.Parse(number);
                for (int i = 1; i < 100; i++)
                {
                    decimal multipliedValue = value * i;
                    if (Math.Abs(Math.Round(multipliedValue) - multipliedValue) <= 0.00000001M)
                    {
                        if (i == 1)
                            return Math.Round(multipliedValue).ToString();
                        return Math.Round(multipliedValue).ToString() + "/" + i.ToString();
                    }
                }
                return value.ToString();
            }
            catch (Exception)
            {
                return number;
            }
        }
        public static string FractionToDecimal(string theNumber)  // CONVERTS A FRACTION (STRING FORMAT) TO A DECIMAL
        {
            int slashIndex = theNumber.IndexOf('/');
            decimal numerator = decimal.Parse(theNumber.Substring(0, (0 + slashIndex)));
            decimal denominator = decimal.Parse(theNumber.Substring(slashIndex + 1, theNumber.Length - (slashIndex + 1)));

            return (numerator / denominator).ToString();
        }
    }
}
