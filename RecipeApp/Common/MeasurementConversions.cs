﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeApp.Common
{
    public static class MeasurementConversions
    {
        public static string WholePlusHalf(string whole, string half)
        {
            double x = double.Parse(whole);
            double y = double.Parse(half) / 2;
            return (x + y).ToString();
        }
        public static string OzToPound(string pounds, string oz)
        {
            double x = double.Parse(pounds);
            double y = double.Parse(oz) / 16;
            return Math.Round((x + y), 1).ToString();
        }
        public static string QuartPlusCup(string quart, string cup)
        {
            double x = double.Parse(quart);
            double y = double.Parse(cup) / 4;
            return Math.Round((x + y), 1).ToString();
        }
        public static string TbspPlusTsp(string tbsp, string tsp)
        {
            double x = double.Parse(tbsp);
            double y = double.Parse(tsp) / 3;
            return Math.Round((x + y), 1).ToString();
        }
    }
}
