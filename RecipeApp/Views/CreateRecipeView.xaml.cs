﻿using RecipeApp.Common;
using RecipeApp.ViewModels;
using RecipeApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace RecipeApp.Views
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class CreateRecipeView : Page
    {
        UserViewModel currentUser;
        RecipeViewModel recipe;

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private RecipeApp.App app = (Application.Current as App);

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public CreateRecipeView()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            currentUser = app.CurrentUser;
            if (e.PageState != null && e.PageState.ContainsKey("currentRecipe"))
                recipe = (RecipeViewModel)e.PageState["currentRecipe"];
            else
                recipe = (RecipeViewModel)e.NavigationParameter;
            DataContext = recipe;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            recipe.RecipeName = RecipeNameTextBox.Text;
            recipe.Description = DescriptionTextBox.Text;
            for (int i = 0; i < recipe.Ingredients.Count; i++)
            {
                if (recipe.Ingredients[i].SelectedCategory != null && recipe.Ingredients[i].SelectedIngredientType != null && recipe.Ingredients[i] != null && 
                    recipe.Ingredients[i].SelectedMeasurement != null)
                {
                    recipe.Ingredients[i].IngredientType = recipe.Ingredients[i].SelectedCategory.Category;
                    recipe.Ingredients[i].IngredientName = recipe.Ingredients[i].SelectedIngredientType.Name;
                    recipe.Ingredients[i].IngredientAmount = recipe.Ingredients[i].SelectedAmount;
                    recipe.Ingredients[i].IngredientMeasurement = recipe.Ingredients[i].SelectedMeasurement.Measurement;
                }
            }
            e.PageState["currentRecipe"] = recipe;
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void AddRowButton_Click(object sender, RoutedEventArgs e)
        {
            recipe.Ingredients.Add(new IngredientViewModel().NewIngredient());
        }
        private void removeIngredientButton_Click(object sender, RoutedEventArgs e)
        {
            IngredientViewModel ingredient = ((Button)sender).Tag as IngredientViewModel;
            recipe.Ingredients.Remove(ingredient);
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Frame pageFrame = null;
            DependencyObject currParent = VisualTreeHelper.GetParent(this);

            while (currParent != null && pageFrame == null)
            {
                pageFrame = currParent as Frame;
                currParent = VisualTreeHelper.GetParent(currParent);
            }
            if (pageFrame != null)
            {
                pageFrame.Navigate(typeof(SelectedRecipeView), pageFrame);
            }
        }

        private bool CheckInputData(RecipeViewModel recipe)
        {
            bool noErrors = true;
            int errorCount = 0;
            int ingredientErrorCount = 0;
            string errorMessage = null;

            if (RecipeNameTextBox.Text == "Recipe Name")
            {
                errorCount++;
                errorMessage += "You must input a Recipe Name.\n";
                noErrors = false;
            }
            for (int i = 0; i < recipe.Ingredients.Count; i++)
            {
                if (recipe.Ingredients[i].SelectedCategory == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedIngredientType == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedMeasurement == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedAmount == null)
                    ingredientErrorCount++;
            }

            if (ingredientErrorCount > 0)
            {
                errorMessage += "You must fill all ingredient fields.";
                noErrors = false;
            }

            if (ingredientErrorCount > 0 || errorCount > 0)
            {
                var messageDialog = new MessageDialog(errorMessage, "Error");
                messageDialog.ShowAsync();
            }

            return noErrors;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckInputData(recipe))
            {
                if (recipe.Id == 0)
                    recipe.Id = recipe.GetNewRecipeId();
                recipe.RecipeName = RecipeNameTextBox.Text;
                recipe.Description = DescriptionTextBox.Text;
                bool saved = recipe.SaveRecipe(recipe);
                for (int i = 0; i < recipe.Ingredients.Count; i++)
                {
                    recipe.Ingredients[i].IngredientType = recipe.Ingredients[i].SelectedCategory.Category;
                    recipe.Ingredients[i].IngredientName = recipe.Ingredients[i].SelectedIngredientType.Name;
                    recipe.Ingredients[i].IngredientAmount = recipe.Ingredients[i].SelectedAmount;
                    recipe.Ingredients[i].IngredientMeasurement = recipe.Ingredients[i].SelectedMeasurement.Measurement;
                }

                string title = string.Empty;
                string message = string.Empty;
                if (saved)
                {
                    title = "Success!";
                    message = "The Recipe was saved successfully.";
                }
                else
                {
                    title = "Error!";
                    message = "The Recipe failed to save.";
                }

                UtilityClass.DialogMessage(title, message);

                if (title != "Error!")
                {
                    Frame pageFrame = null;
                    DependencyObject currParent = VisualTreeHelper.GetParent(this);

                    while (currParent != null && pageFrame == null)
                    {
                        pageFrame = currParent as Frame;
                        currParent = VisualTreeHelper.GetParent(currParent);
                    }
                    if (pageFrame != null)
                        pageFrame.Navigate(typeof(SelectedRecipeView), pageFrame);
                }
            }
        }
    }
}
