﻿using RecipeApp.Models;
using RecipeApp.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RecipeApp.ViewModels
{
    public class IngredientViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ObservableCollection<FoodCategoryViewModel> category;
        private ObservableCollection<IngredientTypeSelectionViewModel> ingredientTypeSelection;
        private ObservableCollection<MeasurementViewModel> measurement;

        private FoodCategoryViewModel selectedCategory;
        private IngredientTypeSelectionViewModel selectedIngredientType;
        private MeasurementViewModel selectedMeasurement;
        private string selectedAmount;

        private int id = 0;
        private int recipes_id = 0;
        private string ingredientType = string.Empty;
        private string ingredientName = string.Empty;
        private string ingredientMeasurement = string.Empty;
        private string ingredientAmount = string.Empty;

        private RecipeApp.App app = (Application.Current as App);

        #region Properties
        public ObservableCollection<FoodCategoryViewModel> Category
        {
            get { return category; }
            set
            {
                if (category == value)
                { return; }
                category = value;
            }
        }
        public ObservableCollection<IngredientTypeSelectionViewModel> IngredientTypeSelection
        {
            get { return ingredientTypeSelection; }
            set
            {
                if (ingredientTypeSelection == value)
                { return; }
                ingredientTypeSelection = value;
                RaisePropertyChanged("IngredientTypeSelection");
            }
        }
        public ObservableCollection<MeasurementViewModel> Measurement
        {
            get { return measurement; }
            set
            {
                if (measurement == value)
                { return; }
                measurement = value;
                RaisePropertyChanged("Measurement");
            }
        }

        public FoodCategoryViewModel SelectedCategory
        {
            get { return this.selectedCategory; }
            set
            {
                this.selectedCategory = value;
                RaisePropertyChanged("SelectedCategory");

                CategoryChosen();
            }
        }
        public IngredientTypeSelectionViewModel SelectedIngredientType
        {
            get { return this.selectedIngredientType; }
            set
            {
                this.selectedIngredientType = value;
                RaisePropertyChanged("SelectedIngredientType");
                
                SetSuggestedAmounts();
            }
        }
        public MeasurementViewModel SelectedMeasurement
        {
            get { return this.selectedMeasurement; }
            set
            {
                this.selectedMeasurement = value;
                RaisePropertyChanged("SelectedMeasurement");
            }
        }
        public string SelectedAmount
        {
            get { return this.selectedAmount; }
            set
            {
                this.selectedAmount = value; // CONVERTS TO FRACTION WHEN CHANGED
                RaisePropertyChanged("SelectedAmount");
            }
        }

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                { return; }
                id = value;
                RaisePropertyChanged("Id");
            }
        }
        public int Recipes_Id
        {
            get { return recipes_id; }
            set
            {
                if (recipes_id == value)
                { return; }
                recipes_id = value;
                RaisePropertyChanged("Recipes_Id");
            }
        }
        public string IngredientType
        {
            get { return ingredientType; }
            set
            {
                if (ingredientType == value)
                { return; }
                ingredientType = value;
                RaisePropertyChanged("IngredientType");
            }
        }
        public string IngredientName
        {
            get { return ingredientName; }
            set
            {
                if (ingredientName == value)
                { return; }
                ingredientName = value;
                RaisePropertyChanged("IngredientName");
            }
        }
        public string IngredientMeasurement
        {
            get { return ingredientMeasurement; }
            set
            {
                if (ingredientMeasurement == value)
                { return; }
                ingredientMeasurement = value;
                RaisePropertyChanged("IngredientMeasurement");
            }
        }
        public string IngredientAmount
        {
            get { return ingredientAmount; }
            set
            {
                if (ingredientAmount == value)
                { return; }
                ingredientAmount = value;
                RaisePropertyChanged("IngredientAmount");
            }
        }
        #endregion

        public IngredientViewModel NewIngredient()
        {
            var ingredient = new IngredientViewModel();
            var category = new FoodCategorysViewModel();
            ingredient.Category = category.GetFoodCategories();
            ingredient.Id = 0;

            return ingredient;
        }
        public IngredientViewModel GetIngredient(int ingredient_id)
        {
            var ingredient = new IngredientViewModel();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var _ingredient = (db.Table<Ingredients>().Where(c => c.Id == ingredient_id)).Single();

                ingredient.Id = _ingredient.Id;
                ingredient.Recipes_Id = _ingredient.Recipes_Id;
                ingredient.IngredientType = _ingredient.IngredientType;
                ingredient.IngredientName = _ingredient.IngredientName;
                ingredient.IngredientAmount = _ingredient.IngredientAmount;
                ingredient.IngredientMeasurement = _ingredient.IngredientMeasurement;

                ingredient.Category = new FoodCategorysViewModel().GetFoodCategories();
                ingredient.SelectedCategory = ingredient.Category.First(c => c.Category == ingredient.IngredientType);
                ingredient.SelectedIngredientType = ingredient.IngredientTypeSelection.First(c => c.Name == ingredient.IngredientName);
                ingredient.SelectedMeasurement = ingredient.Measurement.First(c => c.Measurement == ingredient.IngredientMeasurement);
                ingredient.SelectedAmount = ingredient.IngredientAmount;
            }
            return ingredient;
        }

        public bool SaveIngredient(IngredientViewModel ingredient, int recipe_id)
        {
            bool result = false;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                string change = string.Empty;
                try
                {
                    var existingIngredient = (db.Table<Ingredients>().Where(c => c.Id == ingredient.Id)).SingleOrDefault();
                    if (existingIngredient != null)
                    {
                        existingIngredient.Recipes_Id = recipe_id;
                        existingIngredient.IngredientType = ingredient.IngredientType;
                        existingIngredient.IngredientName = ingredient.IngredientName;
                        existingIngredient.IngredientAmount = ingredient.IngredientAmount;
                        existingIngredient.IngredientMeasurement = ingredient.IngredientMeasurement;
                        int success = db.Update(existingIngredient);
                    }
                    else
                    {
                        var ing = new Ingredients();
                        if (ingredient.Id == 0)
                            ingredient.Id = ingredient.GetNewIngredientId();
                        else
                            ing.Id = ingredient.Id;
                        ing.Recipes_Id = recipe_id;
                        ing.IngredientType = ingredient.SelectedCategory.Category;
                        ing.IngredientName = ingredient.SelectedIngredientType.Name;
                        ing.IngredientAmount = ingredient.SelectedAmount;
                        ing.IngredientMeasurement = ingredient.SelectedMeasurement.Measurement;

                        int success = db.Insert(ing);
                    }
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
        public bool DeleteIngredient(int ingredient_id)
        {
            bool result = false;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var existingIngredient = (db.Table<Ingredients>().Where(c => c.Id == ingredient_id)).Single();
                if (db.Delete<Ingredients>(existingIngredient.Id) > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }
        public int GetNewIngredientId()
        {
            int ingredientId = 0;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var ingredients = db.Table<Ingredients>();
                if (ingredients.Count() > 0)
                    ingredientId = ingredients.Count() + 1;
                else
                    ingredientId = 1;
            }
            return ingredientId;
        }

        private void SetSuggestedAmounts()
        {
            if (SelectedIngredientType != null)
            {
                SelectedMeasurement = Measurement.First(c => c.Measurement == SelectedIngredientType.SuggestedMeasurement);
                SelectedAmount = SelectedIngredientType.SuggestedAmount;
            }
        }
        private void CategoryChosen()
        {
            if (IngredientTypeSelection != null) // Clear IngredientTypeSelection for new selection
                IngredientTypeSelection.Clear();
            if (Measurement != null) // Clear Measurements for new selection
                Measurement.Clear();
            SelectedAmount = ""; // Empty selected amount

            if (SelectedCategory != null) // Re-populate IngredientTypeSelection & Measurement
            {
                IngredientTypeSelection = new IngredientTypeSelectionsViewModel().GetIngredientTypesByCategory(SelectedCategory.Category);
                Measurement = new MeasurementsViewModel().GetMeasurements(SelectedCategory.Category);
            }
        }
    }

    public class FoodCategoryViewModel : ViewModelBase
    {
        private int id;
        private string category;

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                { return; }
                id = value;
                RaisePropertyChanged("Id");
            }
        }
        public string Category
        {
            get { return category; }
            set
            {
                if (category == value)
                { return; }
                category = value;
                RaisePropertyChanged("Category");
            }
        }

        private RecipeApp.App app = Application.Current as App;

        public FoodCategoryViewModel GetFoodCategory(string ingredientType)
        {
            var category = new FoodCategoryViewModel();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var _category = (db.Table<FoodCategory>().Where(c => c.Category == ingredientType)).Single();
                category.Id = _category.Id;
                category.Category = _category.Category;
            }
            return category;
        }
    }
    public class IngredientTypeSelectionViewModel : ViewModelBase
    {
        private int id;
        private string type;
        private string name;
        private string suggestedAmount;
        private string suggestedMeasurement;

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                { return; }
                id = value;
                RaisePropertyChanged("Id");
            }
        }
        public string Type
        {
            get { return type; }
            set
            {
                if (type == value)
                { return; }
                type = value;
                RaisePropertyChanged("Category");
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                { return; }
                name = value;
                RaisePropertyChanged("Name");
            }
        }
        public string SuggestedAmount
        {
            get { return suggestedAmount; }
            set
            {
                if (suggestedAmount == value)
                { return; }
                suggestedAmount = value;
                RaisePropertyChanged("SuggestedAmount");
            }
        }
        public string SuggestedMeasurement
        {
            get { return suggestedMeasurement; }
            set
            {
                if (suggestedMeasurement == value)
                { return; }
                suggestedMeasurement = value;
                RaisePropertyChanged("SuggestedMeasurement");
            }
        }

        private RecipeApp.App app = Application.Current as App;

        public IngredientTypeSelectionViewModel GetIngredientType(string ingredientType, string ingredientName)
        {
            var _ingredientType = new IngredientTypeSelectionViewModel();
            try
            {
                ingredientType = UtilityClass.GetCategory(ingredientType);
                using (var db = new SQLite.SQLiteConnection(app.DBPath))
                {
                    var _IngredientType = (db.Table<AllIngredints>().Where(c => c.Name == ingredientName && c.Type == ingredientType)).Single();
                    _ingredientType.Id = _IngredientType.Id;
                    _ingredientType.Type = _IngredientType.Type;
                    _ingredientType.Name = _IngredientType.Name;
                    _ingredientType.SuggestedAmount = _IngredientType.SuggestedAmount;
                    _ingredientType.SuggestedMeasurement = _IngredientType.SuggestedMeasurement;
                }
            }
            catch (Exception)
            {
                // Do nothing....
            }
            return _ingredientType;
        }
    }
    public class MeasurementViewModel : ViewModelBase
    {
        private int id;
        private string measurement;
        private string type;

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                { return; }
                id = value;
                RaisePropertyChanged("Id");
            }
        }
        public string Type
        {
            get { return type; }
            set
            {
                if (type == value)
                { return; }
                type = value;
                RaisePropertyChanged("Type");
            }
        }
        public string Measurement
        {
            get { return measurement; }
            set
            {
                if (measurement == value)
                { return; }
                measurement = value;
                RaisePropertyChanged("Category");
            }
        }

        private RecipeApp.App app = Application.Current as App;

        public MeasurementViewModel GetMeasurement(string category, string meas)
        {
            var measurement = new MeasurementViewModel();
            try
            {
                category = UtilityClass.GetCategory(category);
                using (var db = new SQLite.SQLiteConnection(app.DBPath))
                {
                    var _measurement = (db.Table<Measurement>().Where(c => c.Type == category && c.Meas == meas)).Single();
                    measurement.Id = _measurement.Id;
                    measurement.Type = _measurement.Type;
                    measurement.Measurement = _measurement.Meas;
                }
            }
            catch (Exception)
            {
                // Do nothing...
            }
            return measurement;
        }
    }
}
