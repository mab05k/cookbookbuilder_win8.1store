﻿using RecipeApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RecipeApp.ViewModels
{
    public class UsersViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ObservableCollection<UserViewModel> users;
        public ObservableCollection<UserViewModel> Users
        {
            get { return users; }
            set
            {
                users = value;
                RaisePropertyChanged("Users");
            }
        }

        private RecipeApp.App app = (Application.Current as App);

        //public ObservableCollection<UserViewModel> GetUser()
        //{
        //    users = new ObservableCollection<UserViewModel>();
        //    using (var db = new SQLite.SQLiteConnection(app.DBPath))
        //    {
        //        var query = (db.Table<User>());
        //        foreach (var _user in query)
        //        {
        //            var user = new UserViewModel().GetUser(_user.Id);
        //            users.Add(user);
        //        }
        //    }
        //    return users;
        //}
        public ObservableCollection<UserViewModel> GetUsers()
        {
            users = new ObservableCollection<UserViewModel>();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var query = (db.Table<User>().OrderBy(c => c.UserName));
                foreach (var user in query)
                {
                    var _user = new UserViewModel();
                    _user.Id = user.Id;
                    _user.UserName = user.UserName;
                    users.Add(_user);
                }
            }
            return users;
        }
    }
}
