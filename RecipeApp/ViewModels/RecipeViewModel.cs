﻿using RecipeApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RecipeApp.ViewModels
{
    public class RecipeViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private int id = 0;
        private int user_id = 0;
        private string recipeName = string.Empty;
        private DateTime dateCreated = DateTime.Now;
        private int visits = 0;
        private string description = string.Empty;
        private ObservableCollection<IngredientViewModel> ingredients;

        #region Properties
        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                { return; }
                id = value;
                RaisePropertyChanged("Id");
            }
        }
        public int User_Id
        {
            get { return user_id; }
            set
            {
                if (user_id == value)
                { return; }
                user_id = value;
                RaisePropertyChanged("User_Id");
            }
        }
        public string RecipeName
        {
            get { return recipeName; }
            set
            {
                if (recipeName == value)
                { return; }
                recipeName = value;
                RaisePropertyChanged("RecipeName");
            }
        }
        public DateTime DateCreated
        {
            get { return dateCreated; }
            set
            {
                if (dateCreated == value)
                { return; }
                dateCreated = value;
                RaisePropertyChanged("DateCreated");
            }
        }
        public int Visits
        {
            get { return visits; }
            set
            {
                if (visits == value)
                { return; }
                visits = value;
                RaisePropertyChanged("Visits");
            }
        }
        public string Description
        {
            get { return description; }
            set
            {
                if (description == value)
                { return; }
                description = value;
                RaisePropertyChanged("Description");
            }
        }
        public ObservableCollection<IngredientViewModel> Ingredients
        {
            get { return ingredients; }
            set
            {
                ingredients = value;
                RaisePropertyChanged("Ingredients");
            }
        }
        #endregion

        private RecipeApp.App app = (Application.Current as App);

        public RecipeViewModel NewRecipe()
        {
            var _recipe = new RecipeViewModel();
            _recipe.Id = _recipe.GetNewRecipeId();
            _recipe.User_Id = app.CurrentUser.Id;
            _recipe.RecipeName = "";
            _recipe.DateCreated = DateTime.Now;
            _recipe.Visits = 0;
            _recipe.Description = "";
            _recipe.Ingredients = new ObservableCollection<IngredientViewModel>();

            return _recipe;
        }
        public RecipeViewModel GetRecipe(int recipe_id)
        {
            var recipe = new RecipeViewModel();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var _recipe = (db.Table<Recipes>().Where(c => c.Id == recipe_id)).Single();
                recipe.Id = _recipe.Id;
                recipe.User_Id = _recipe.User_Id;
                recipe.RecipeName = _recipe.RecipeName;
                recipe.DateCreated = _recipe.Date;
                recipe.Visits = _recipe.Visits;
                recipe.Description = _recipe.Description;
                recipe.Ingredients = new IngredientsViewModel().GetIngredients(_recipe.Id);
            }
            return recipe;
        }
        public bool SaveRecipe(RecipeViewModel recipe)
        {
            bool result = false;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                try
                {
                    var existingRecipe = (db.Table<Recipes>().Where(c => c.Id == recipe.Id)).SingleOrDefault();
                    if (existingRecipe != null)
                    {
                        existingRecipe.RecipeName = recipe.RecipeName;
                        existingRecipe.Visits = recipe.Visits;
                        existingRecipe.Description = recipe.Description;

                        var existingIngredients = new IngredientsViewModel().GetIngredients(recipe.Id);
                        if (existingIngredients.Count > recipe.Ingredients.Count)
                        {                            
                            for (int i = 0; i < existingIngredients.Count; i++) {
                                bool extra = true;
                                bool deleted = false;
                                for (int j = 0; j < recipe.Ingredients.Count; j++) {
                                    if (existingIngredients[i].Id == recipe.Ingredients[j].Id)
                                        extra = false;
                                }
                                if (extra)
                                    deleted = new IngredientViewModel().DeleteIngredient(existingIngredients[i].Id);
                            }
                        }
                        for (int i = 0; i < recipe.Ingredients.Count; i++) {
                            bool ingredient = new IngredientViewModel().SaveIngredient(recipe.Ingredients[i], recipe.Id);
                            if (!ingredient) {
                                return result = false;
                            }
                        }

                        int success = db.Update(existingRecipe);
                        result = true;
                    }
                    else
                    {
                        for (int i = 0; i < recipe.Ingredients.Count; i++)
                        {
                            recipe.Ingredients[i].Id = new IngredientViewModel().GetNewIngredientId();
                            bool ingredient = new IngredientViewModel().SaveIngredient(recipe.Ingredients[i], recipe.Id);
                            if (!ingredient)
                            {
                                return result = false;
                            }
                        }
                        var _recipe = new Recipes()
                        {
                            Id = recipe.Id,
                            User_Id = app.CurrentUser.Id,
                            RecipeName = recipe.RecipeName,
                            Date = recipe.DateCreated,
                            Visits = recipe.Visits,
                            Description = recipe.Description
                        };
                        int success = db.Insert(_recipe);
                        result = true;

                        app.CurrentUser.Recipes.Add(recipe);
                    }
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
        public bool DeleteRecipe (int recipe_id)
        {
            bool result = false;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var existingRecipe = (db.Table<Recipes>().Where(c => c.Id == recipe_id)).Single();
                if (db.Delete<Recipes>(existingRecipe.Id) > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public int GetNewRecipeId()
        {
            int recipeId = 0;
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var recipes = db.Table<Recipes>();
                if (recipes.Count() > 0)
                {
                    recipeId = recipes.Count() + 1;
                }
                else
                {
                    recipeId = 1;
                }
            }
            return recipeId;
        }
    }
}
