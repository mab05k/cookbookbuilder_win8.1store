﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace RecipeApp.Common
{
    public static class UtilityClass
    {
        public static string FormatToFraction(string amount)
        {
            try
            {
                decimal num = decimal.Parse(amount);
                amount = FractionConverter.ConvertToFraction(amount);
                return amount;
            }
            catch (Exception)
            {
                return amount;
            }
        }
        public static string FormatToDecimal(string amount)
        {
            try
            {
                decimal num = decimal.Parse(amount);
                return amount;
            }
            catch (Exception)
            {
                amount = FractionConverter.FractionToDecimal(amount);
                return amount;
            }
        }
        public static string GetCategory(string category)
        {
            switch (category)
            {
                case "Meat":
                    category = "Meat";
                    break;
                case "Vegetables":
                    category = "Vegetable";
                    break;
                case "Fruit":
                    category = "Fruit";
                    break;
                case "Grains":
                    category = "Grain";
                    break;
                case "Nuts":
                    category = "Nut";
                    break;
                case "Dairy":
                    category = "Dairy";
                    break;
                case "Herbs & Spices":
                    category = "Herb";
                    break;
                case "Legumes":
                    category = "Legume";
                    break;
                case "Oils":
                    category = "Oil";
                    break;
                case "Vinegars & Stocks":
                    category = "VinegarStock";
                    break;
                case "Specialty Items":
                    category = "Specialty";
                    break;
                default:
                    break;
            }

            return category;
        }
        public static string MeasurementNameConverter(string measurementName)
        {
            switch (measurementName)
            {
                case "lb":
                    measurementName = "Lb.";
                    break;
                case "oz":
                    measurementName = "Oz.";
                    break;
                case "count":
                    measurementName = "Count";
                    break;
                case "clove":
                    measurementName = "Clove";
                    break;
                case "cup":
                    measurementName = "Cup";
                    break;
                case "slice":
                    measurementName = "Slice";
                    break;
                case "tbsp":
                    measurementName = "Tbsp";
                    break;
                case "tsp":
                    measurementName = "Tsp";
                    break;
                case "gallon":
                    measurementName = "Gallon";
                    break;
                case "sprig":
                    measurementName = "Sprig";
                    break;
                case "bunch":
                    measurementName = "Bunch";
                    break;
                default:
                    break;
            }
            return measurementName;
        }
        public async static void DialogMessage(string title, string message)
        {
            var messageDialog = new MessageDialog(message, title);
            await messageDialog.ShowAsync();
        }
    }
}
